<?php

/**
 * @file
 * Primary module hooks for Identicon module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add the default personal contact setting on the user settings page.
 *
 * @see \Drupal\user\AccountSettingsForm
 */
function identicon_form_user_admin_settings_alter(&$form, FormStateInterface $form_state) {
  $form['personalization'] = [
    '#type' => 'details',
    '#title' => t('Identicon settings'),
    '#open' => TRUE,
    '#weight' => 0,
  ];

  $form['personalization']['identicon']['identicon_source'] = [
    '#type' => 'select',
    '#title' => t('Identicon data source'),
    '#description' => t('Select what data should be the basis of generating the identicon. Changing this setting will change all existing identicons.'),
    '#options' => identicon_source_options(),
    '#default_value' => \Drupal::configFactory()
      ->getEditable('identicon.settings')
      ->get('identicon_source') ?? 'mail',
  ];

  $form['personalization']['identicon']['identicon_blocks'] = [
    '#type' => 'select',
    '#title' => t('Blocks in identicons'),
    '#description' => t('The number of blocks on each side of the identicon.'),
    '#options' => identicon_blocks_options(),
    '#default_value' => \Drupal::configFactory()
      ->getEditable('identicon.settings')
      ->get('identicon_blocks') ?? 5,
  ];

  $form['personalization']['identicon']['identicon_resolution'] = [
    '#type' => 'select',
    '#title' => t('Resolution of identicons'),
    '#description' => t('The pixel resolution of the generated identicon images. The displayed image may have a resolution different that this if an image style is applied to it.'),
    '#options' => identicon_resolution_options(),
    '#default_value' => \Drupal::configFactory()
      ->getEditable('identicon.settings')
      ->get('identicon_resolution') ?? 1,
  ];

  // Add submit handler to save contact configuration.
  $form['#submit'][] = 'identicon_form_user_admin_settings_submit';
}

/**
 * Form submission handler for user_admin_settings().
 */
function identicon_form_user_admin_settings_submit($form, FormStateInterface $form_state) {
  \Drupal::configFactory()->getEditable('identicon.settings')
    ->set('identicon_enabled', $form_state->getValue('identicon_enabled'))
    ->set('identicon_source', $form_state->getValue('identicon_source'))
    ->set('identicon_blocks', $form_state->getValue('identicon_blocks'))
    ->set('identicon_resolution', $form_state->getValue('identicon_resolution'))
    ->save();
}

/**
 * Returns options for the data source field.
 *
 * @return
 *   Array suitable for the #options key of a form element.
 */
function identicon_source_options() {
  return [
    'mail' => 'Email address',
    'uid' => 'User ID',
  ];
}

/**
 * Returns options for the block count field.
 *
 * @return
 *   Array suitable for the #options key of a form element.
 */
function identicon_blocks_options() {
  return [
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
  ];
}

/**
 * Returns options for the resolution field.
 *
 * @return
 *   Array suitable for the #options key of a form element.
 */
function identicon_resolution_options() {
  return [
    '150',
    '300',
    '600',
    '1200',
  ];
}

/**
 * Implements hook_user_update().
 */
function identicon_user_update(EntityInterface $entity) {
  if (!empty($entity->get('user_picture')->getValue())) {
    identicon_delete_images($entity);
  }
}

/**
 * Implements hook_user_delete().
 */
function identicon_user_delete(EntityInterface $entity) {
  identicon_delete_images($entity);
}

/**
 * Delete identicon images for a user.
 *
 * @param $account
 *   A user object.
 */
function identicon_delete_images(User $user) {
  $hash = identicon_user_hash($user);

  $delete_path = \Drupal::config('system.file')
      ->get('default_scheme') . '://identicon/identicon-' . $hash . '-*';

  foreach (glob(\Drupal::service('file_system')
    ->realpath($delete_path)) as $filename) {
    unlink($filename);
  }
}

/**
 * Returns the user's identicon file.
 */
function identicon_get_identicon_file(User $user) {
  $image_path = identicon_identicon_image_path($user);
  if (!$image_path) {
    return NULL;
  }

  $images = \Drupal::entityTypeManager()
    ->getStorage('file')
    ->loadByProperties(['uri' => $image_path]);
  /** @var \Drupal\file\FileInterface|null $file */
  $image = reset($images) ?: NULL;

  return $image;
}

/**
 * Generates a hash for a user account that can be used as
 * the basis of identicon generation.
 *
 * @param User $user
 *   User object.
 *
 * @return
 *   Hash as string.
 */
function identicon_user_hash(User $user) {
  $source = \Drupal::configFactory()
    ->getEditable('identicon.settings')
    ->get('identicon_source');
  switch ($source) {
    case 'uid':
      $data = $user->id();
      break;

    default:
      $data = $user->getEmail();
      break;
  }

  return hash('sha256', strtolower(trim($data)));
}

/**
 * Returns the path to the identicon for a user.
 *
 * @param User $user
 *   A user object.
 *
 * @return string
 *   File path of the identicon image; or FALSE on error.
 */
function identicon_identicon_image_path(User $user) {
  $config = \Drupal::configFactory()->getEditable('identicon.settings');
  $hash = identicon_user_hash($user);

  $blocks_options = identicon_blocks_options();
  $blocks_selected = !empty($config->get('identicon_blocks')) ? $config->get('identicon_blocks') : 5;
  $blocks = $blocks_options[$blocks_selected];

  $resolution_options = identicon_resolution_options();
  $resolution_selected = !empty($config->get('identicon_resolution')) ? $config->get('identicon_resolution') : 1;
  $resolution = $resolution_options[$resolution_selected];

  $dir = \Drupal::config('system.file')->get('default_scheme') . '://identicon';
  \Drupal::service('file_system')->prepareDirectory($dir, FileSystem::CREATE_DIRECTORY);
  $image_path = $dir . '/identicon-' . $hash . '-' . $resolution . '-' . $blocks . '.png';

  if (!file_exists($image_path)) {
    $result = identicon_generate($hash, $blocks, $resolution, $image_path);

    if (!$result) {
      \Drupal::logger('identicon')
        ->error('Cannot save identicon to file %file.', ['%file' => $image_path]);
      return FALSE;
    }
  }

  return $image_path;
}

/**
 * Generates and saves an identicon.
 *
 * @param $hash
 *   The hash that serves as the data source for generating the identicon.
 * @param $blocks
 *   The number of blocks on each side of the identicon.
 * @param $resolution
 *   The resolution of the generated image. The image is square, so only one
 *   dimension is needed.
 * @param $image_path
 *   The target path where the image should be saved.
 *
 * @return
 *   TRUE on success; FALSE on error.
 */
function identicon_generate($hash, $blocks, $resolution, $image_path) {

  // First 6 digits determine the color.
  $color = substr($hash, 0, 6);

  // Next digit determines the mirroring. Possible values:
  // 0: horizontal
  // 1: vertical
  $mirroring = hexdec(substr($hash, 6, 1)) % 2;

  // Remove the digits we have used.
  $hash = substr($hash, 7);

  // Calculate the number of bits we need (only fill half of the blocks,
  // because it will be mirrored).
  $half_size = ceil($blocks / 2);
  $number_of_bits = $half_size * $blocks;

  $bits = array_fill(0, $number_of_bits, 0);
  $counter = 0;

  // Gather the bits from the hash.
  do {

    for ($i = 0; $i < strlen($hash); $i++) {
      $dec = hexdec($hash[$i]);
      $bin = str_pad(decbin($dec), 4, '0', STR_PAD_LEFT);

      for ($j = 0; $j < strlen($bin); $j++) {
        $index = $counter % $number_of_bits;

        // If we have more bits than we need, we iterate over
        // the existing bits and xor the extra ones so that any
        // bit change will cause a change in the image.
        $bits[$index] = ($bits[$index] xor $bin[$j]) ? 1 : 0;
        $counter++;
      }

    }

    // If there are not enough bits in the hash to draw the full
    // image, reuse previous bits.
  } while ($counter < $number_of_bits);

  // Build the image.
  $image = imagecreatetruecolor($resolution, $resolution);

  $color = imagecolorallocate($image, hexdec(substr($color, 0, 2)), hexdec(substr($color, 2, 2)), hexdec(substr($color, 4, 2)));
  $background = imagecolorallocate($image, 255, 255, 255);

  imagefilledrectangle($image, 0, 0, $resolution, $resolution, $background);

  $block_size = floor($resolution / $blocks);
  $counter = 0;

  for ($i = 0; $i < $half_size; $i++) {
    for ($j = 0; $j < $blocks; $j++) {

      if (!$bits[$counter++]) {
        // Bit is not set, keep the background color in this block.
        continue;
      }

      switch ($mirroring) {
        case 0:
          // Mirroring horizontally. $i represents x axis, $j is y axis.

          $x1 = ($i) * $block_size;
          $y1 = ($j) * $block_size;
          $x2 = ($i + 1) * $block_size;
          $y2 = ($j + 1) * $block_size;

          // Last row fills in the extra space in case $resolution % $block != 0.
          if ($j == ($blocks - 1) && $y2 != $resolution) {
            $y2 = $resolution;
          }

          imagefilledrectangle($image, $x1, $y1, $x2, $y2, $color);

          // Fill the mirrored block.
          $x1 = ($blocks - 1 - $i) * $block_size;
          $x2 = ($blocks - 1 - $i + 1) * $block_size;

          if ($i == 0 && $x2 != $resolution) {
            $x2 = $resolution;
          }

          imagefilledrectangle($image, $x1, $y1, $x2, $y2, $color);

          break;

        case 1:
          // Mirroring vertically. $i represents y axis, $j is x axis.

          $x1 = ($j) * $block_size;
          $y1 = ($i) * $block_size;
          $x2 = ($j + 1) * $block_size;
          $y2 = ($i + 1) * $block_size;

          // Last row fills in the extra space in case $resolution % $block != 0.
          if ($j == ($blocks - 1) && $x2 != $resolution) {
            $x2 = $resolution;
          }

          imagefilledrectangle($image, $x1, $y1, $x2, $y2, $color);

          // Fill the mirrored block.
          $y1 = ($blocks - 1 - $i) * $block_size;
          $y2 = ($blocks - 1 - $i + 1) * $block_size;

          if ($i == 0 && $y2 != $resolution) {
            $y2 = $resolution;
          }

          imagefilledrectangle($image, $x1, $y1, $x2, $y2, $color);

          break;
      }

    }
  }

  $file = File::create([
    'filename' => basename($image_path),
    'uri' => $image_path,
    'status' => 1,
    'uid' => 1,
  ]);
  $file->save();

  return imagepng($image, \Drupal::service('file_system')->realpath($file->getFileUri()));
}
