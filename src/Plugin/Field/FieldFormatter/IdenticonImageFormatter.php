<?php

namespace Drupal\identicon\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the 'identicon image' formatter.
 *
 * @FieldFormatter(
 *   id = "identicon_image",
 *   label = @Translation("Identicon Image"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class IdenticonImageFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entity = $items->getParent()->getEntity();

    if ($items->isEmpty() && $entity instanceof User) {
      $file = identicon_get_identicon_file($entity);

      if ($file) {
        // Clone the FieldItemList into a runtime-only object for the formatter,
        // so that the fallback image can be rendered without affecting the
        // field values in the entity being rendered.
        $items = clone $items;
        $items->setValue([
          'target_id' => $file->id(),
          'alt' => 'Identicon',
          'title' => 'Identicon',
          'width' => NULL,
          'height' => NULL,
          'entity' => $file,
          '_loaded' => TRUE,
          '_is_default' => TRUE,
        ]);
        $file->_referringItem = $items[0];
      }
    }

    return parent::viewElements($items, $langcode);
  }

}
